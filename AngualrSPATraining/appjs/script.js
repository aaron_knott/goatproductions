﻿// also include ngRoute for all our routing needs
var scotchApp = angular.module('scotchApp', ['ngRoute', 'ngAnimate']);

// configure our routes
scotchApp.config(function ($routeProvider) {
    $routeProvider

     // route for the home page
            .when('/', {
                templateUrl: 'pages/home.html',
                controller: 'mainController'
            })

            // route for the about page
            .when('/about', {
                templateUrl: 'pages/about.html',
                controller: 'aboutController'
            })

            // route for the contact page
            .when('/contact', {
                templateUrl: 'pages/contact.html',
                controller: 'contactController'
            })

            .when('/portfolio', {
                templateUrl: 'pages/portfolio.html',
                controller: 'portfolioController'
            });
})

// create the controller and inject Angular's $scope
scotchApp.controller('mainController', function ($scope) {
    // create a message to display in our view
    $scope.pageClass = 'home-page';
    $scope.message = 'Everyone come and see how good I look!';
});

scotchApp.controller('aboutController', function ($scope) {
    $scope.pageClass = 'about-page';
    $scope.message = 'Look! I am an about page.';
});

scotchApp.controller('contactController', function ($scope) {
    $scope.pageClass = 'contat-page';
    $scope.message = 'Contact us! JK. This is just a demo.';
});

scotchApp.controller('portfolioController', function ($scope) {
    $scope.pageClass = 'portfolio-page';
    $scope.message = 'My sweet portfolio';

    $scope.projects = [
        {
            name: "King and Taylor",
            description: "Orchard website",
            technologies: [
                "Javascript",
                "Orchard CMS",
                "Sass",
                "HTML"],
            images: [],
    link: 'http://www.kingandtaylor.co.uk'
},
        {
            name: "i.e. taxguard",
            description: "Orchard website",
            technologies: [
                "Javascript",
                "Orchard CMS",
                "Sass",
                "HTML"],
                images: [],
link: 'http://www.ietaxguard.co.uk'
}

]
});