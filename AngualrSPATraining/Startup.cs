﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AngualrSPATraining.Startup))]
namespace AngualrSPATraining
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
